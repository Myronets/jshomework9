// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//     Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался
//     конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано,
//     какой текст должен отображаться для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
//     При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

//
// document.querySelector('.tabs-title').classList.add('active');
// document.querySelector('.tabs-content-item').classList.add('active');
//
//
// let tabsTitle = document.querySelectorAll('.tabs-title'),
//     tabsContent = document.querySelectorAll('.tabs-content li');
//
// tabsContent.forEach((e,index) => e.dataset.index = index);
//
// for (let el of tabsTitle) {
//     el.addEventListener('click', e=>{
//         e.preventDefault();
//
//         document.querySelector('.tabs-title.active').classList.remove('active');
//         document.querySelector('.tabs-content-item.active').classList.remove('active');
//
//         el.classList.add('active');
//         const index = [...tabsTitle].indexOf(el);
//
//         const content = [...tabsContent].filter(el => el.getAttribute('data-index') == index);
//         content[0].classList.add('active');
//     })
// }

/***************************** hw 14 *******************************/
// Задание
// Переделать домашнее задание 9 (табы), используя jQuery

$('.tabs-title').not('.active').click(function(){
    let index = $(this).index();
    let content = $('.tabs-content-item').eq(index);
    $(this).addClass('active').siblings().removeClass('active');
    $('.tabs-content-item').removeClass('active').eq(index).addClass('active');
});

$('.tabs-title:first').addClass('active');
$('.tabs-content-item:first').addClass('active');

